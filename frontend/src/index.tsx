import * as React from "react";
import * as ReactDOM from "react-dom";
import { useStrict } from "mobx";
import DevTools from "mobx-react-devtools";

import { App } from "./View";

useStrict(true);
ReactDOM.render(
    <div className="container">
        <DevTools />
        <App />
    </div>,
    document.getElementById("app"));
