export interface ILoginResponse {
    userId: number
}

export function login(username: string): Promise<{ userId: number }> {
    return post("api/login", { username });
}

export function send(userId: number, message: string): Promise<{}> {
    return post("api/messages", { userId, message });
}

export function getMessages(lastSeen?: number): Promise<IGetMessagesResponse> {
    if (typeof lastSeen === "undefined") {
        return get("api/messages");
    } else {
        return get(`api/messages?lastSeen=${lastSeen}`)
    }
}

export interface IGetMessagesResponse {
    messages: IChatMessage[]
}

export interface IChatMessage {
    id: number;
    sender: string;
    message: string;
}

async function fetchJson(promise: Promise<Response>) {
    let response: Response;
    try {
        response = await promise;
    } catch (e) {
        return Promise.reject(e.statusText);
    }
    if (!response.ok) {
        return Promise.reject(await response.text());
    }
    return response.json();
}

function get(url: string) {
    return fetchJson(fetch(url, {
        headers: {
            "Content-Type": "application/json"
        },
        method: "GET"
    }));
}

function post(url: string, body: any) {
    return fetchJson(fetch(url, {
        body: JSON.stringify(body),
        headers: {
            "Content-Type": "application/json"
        },
        method: "POST"
    }));
}
