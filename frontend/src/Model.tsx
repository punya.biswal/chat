import { action, computed, observable } from "mobx";

import * as Api from "./Api";

export class Model {
    @observable page: "login" | "chat" | "error" = "login";

    @observable username = "";
    @observable sending = false;

    @observable userId: number;
    @observable message = "";
    @observable history = new HistoryModel();

    @observable error: string;

    @action async login() {
        this.sending = true;
        setInterval(() => this.history.refresh(), 1000);
        try {
            let {userId} = await Api.login(this.username);
            this.loginSucceeded(userId);
            await this.history.loadInitialMessages();
        } catch (e) {
            this.failed(e);
        }
    }

    @action private loginSucceeded(userId: number) {
        this.sending = false;
        this.page = "chat";
        this.userId = userId;
    }

    @action async send() {
        this.sending = true;
        try {
            await Api.send(this.userId, this.message);
            // wait for refresh before clearing the message displayed
            await this.history.refresh();
            this.sendSucceeded();
        } catch (e) {
            this.failed(e);
        }
    }

    @action private sendSucceeded() {
        this.sending = false;
        this.message = "";
    }

    @action setMessage(message: string) {
        this.message = message;
    }

    @action setUsername(username: string) {
        this.username = username;
    }

    @action private failed(error: string) {
        this.sending = false;
        this.page = "error";
        this.error = error;
    }
}

export class HistoryModel {
    @observable loaded = false;
    @observable messages: Api.IChatMessage[] = [];

    @computed get lastSeen() {
        let messages = this.messages;
        if (messages.length === 0) {
            return -1;
        }
        return messages[messages.length - 1].id;
    }

    @action async loadInitialMessages() {
        let {messages} = await Api.getMessages();
        this.initialLoadSucceeded(messages);
    }

    @action private initialLoadSucceeded(delta: Api.IChatMessage[]) {
        this.messages = delta;
        this.loaded = true;
    }

    @action async refresh() {
        let requestedLastSeen = this.lastSeen;
        let {messages} = await Api.getMessages(requestedLastSeen);
        this.refreshSucceeded(requestedLastSeen, messages);
    }

    @action private refreshSucceeded(requestedLastSeen: number, delta: Api.IChatMessage[]) {
        if (requestedLastSeen === this.lastSeen) {
            this.messages.push(...delta);
        }
    }
}
