import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";

import { Model, HistoryModel } from "./Model";

@observer
export class App extends React.Component<{}, {}> {

    @observable private model = new Model();

    render() {
        switch (this.model.page) {
            case "login":
                return this.loginPage();
            case "chat":
                return this.chatPage();
            case "error":
                return this.errorPage();
        }
    }

    private loginPage() {
        return (
            <div>
                <div className="page-header">
                    <h1>Welcome</h1>
                </div>
                <form onSubmit={this.login} className="form-inline">
                    <div className="input-group">
                        <span className="input-group-addon">
                            <span className="glyphicon glyphicon-user" />
                        </span>
                        <input
                            className="form-control"
                            autoFocus={true}
                            placeholder="Username"
                            onChange={this.setUsername}
                            value={this.model.username}
                            readOnly={this.model.sending}
                            />
                        <span className="input-group-btn">
                            <input
                                className="btn btn-primary"
                                type="submit"
                                value="Sign in"
                                disabled={this.model.username === ""}
                                />
                        </span>
                    </div>
                </form>
            </div>
        );
    }

    private chatPage() {
        return (
            <div>
                <div className="page-header">
                    <h1>
                        Chat
                    </h1>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <History model={this.model.history} />
                    </div>
                </div>
                <form onSubmit={this.send}>
                    <div className="input-group">
                        <span className="input-group-addon"><User username={this.model.username} /></span>
                        <input
                            className="form-control"
                            autoFocus={true}
                            placeholder="Message"
                            onChange={this.setMessage}
                            value={this.model.message}
                            readOnly={this.model.sending}
                            />
                        <span className="input-group-btn">
                            <input
                                className="btn btn-primary"
                                type="submit"
                                value="Send"
                                disabled={this.model.message === ""}
                                />
                        </span>
                    </div>
                </form>
            </div>
        );
    }

    private errorPage() {
        return (
            <div>
                <div className="page-header">
                    <h1>Error</h1>
                </div>
                <pre>{this.model.error}</pre>
            </div>
        );
    }

    private setMessage = (evt: React.FormEvent<HTMLInputElement>) => {
        this.model.setMessage((evt.target as HTMLInputElement).value);
    }

    private setUsername = (evt: React.FormEvent<HTMLInputElement>) => {
        this.model.setUsername((evt.target as HTMLInputElement).value);
    }

    private login = (evt: React.FormEvent<HTMLFormElement>) => {
        evt.preventDefault();
        this.model.login();
    }

    private send = (evt: React.FormEvent<HTMLFormElement>) => {
        evt.preventDefault();
        this.model.send();
    }
}

@observer
class History extends React.Component<{model: HistoryModel}, {}> {

    render() {
        if (!this.props.model.loaded) {
            return <div>Loading&hellip;</div>;
        }
        return <div>{this.rows()}</div>;
    }

    private rows() {
        let groups: {sender: string, messages: string[]}[] = [];
        for (let {sender, message} of this.props.model.messages) {
            if (groups.length === 0 || groups[groups.length - 1].sender !== sender) {
                groups.push({sender, messages: []});
            }
            groups[groups.length - 1].messages.push(message);
        }
        return groups.map(({sender, messages}, i) => (
            <div className="row" key={i}>
                <div className="panel panel-default">
                    <div className="panel-heading"><User username={sender} /></div>
                    <ul className="list-group">
                        {messages.map((message, j) => <li key={j} className="list-group-item">{message}</li>)}
                    </ul>
                </div>
            </div>
        ));
    }
}

const User = ({username}: {username: string}) => (
    <span>
        <span className="glyphicon glyphicon-user" />&nbsp;{username}
    </span>
);
