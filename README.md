Chat
====

Running
-------

```
$ git clone https://gitlab.com/punya.biswal/chat.git
$ cd chat
```

In one terminal:
```
$ cd backend;
$ ./gradlew run
```

In the other,
```
$ cd frontend; npm install
$ npm start
```

Open http://localhost:8080 in Chrome.

Overview
--------

* Data is stored in an on-disk H2 database
* The backend uses Dropwizard and associated libraries
* The frontend uses React and MobX

Missing
-------

- [ ] Add tests
- [ ] Improve notifications, e.g. using websockets
- [ ] Use an external DB such as PostgreSQL
- [ ] Keep message box within view and scroll history
- [ ] Polyfill features missing from Chrome
