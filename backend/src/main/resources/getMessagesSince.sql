SELECT messages.id, users.username AS sender, messages.message
FROM messages
INNER JOIN users
ON messages.userId = users.id
WHERE messages.id > :lastSeen
ORDER BY messages.id ASC;
