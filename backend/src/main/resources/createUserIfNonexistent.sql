MERGE INTO users (id, username)
KEY (username)
VALUES
  ((
     SELECT id
     FROM users u2
     WHERE u2.username = :username
   ),
   :username
  );
