CREATE TABLE IF NOT EXISTS messages(
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  userId INTEGER NOT NULL REFERENCES users(id),
  message VARCHAR
);
