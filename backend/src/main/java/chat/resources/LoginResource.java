package chat.resources;

import chat.api.LoginRequest;
import chat.api.LoginResponse;
import chat.jdbi.UserDAO;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("login")
@Produces(MediaType.APPLICATION_JSON)
public class LoginResource {
    private final UserDAO userDao;

    public LoginResource(UserDAO userDao) {
        this.userDao = userDao;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public LoginResponse login(@NotNull @Valid LoginRequest request) {
        String username = request.username;
        long userId = userDao.createUserIfNonexistent(username);
        if (userId == 0) {
            userId = userDao.getUserId(username);
        }
        return new LoginResponse(userId);
    }
}
