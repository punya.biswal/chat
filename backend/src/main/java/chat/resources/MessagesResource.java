package chat.resources;

import chat.api.ChatMessage;
import chat.api.GetMessagesResponse;
import chat.api.SendMessageRequest;
import chat.api.SendMessageResponse;
import chat.jdbi.MessagesDAO;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("messages")
@Produces(MediaType.APPLICATION_JSON)
public class MessagesResource {
    private final MessagesDAO messagesDao;

    public MessagesResource(MessagesDAO messagesDao) {
        this.messagesDao = messagesDao;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public SendMessageResponse send(SendMessageRequest request) {
        long messageId = this.messagesDao.addMessage(request.userId, request.message);
        return new SendMessageResponse(messageId);
    }

    @GET
    public GetMessagesResponse listSince(@QueryParam("lastSeen") @DefaultValue("-1") long lastSeen) {
        List<ChatMessage> messages = messagesDao.getMessagesSince(lastSeen);
        return new GetMessagesResponse(messages);
    }
}
