package chat.api;

public class SendMessageResponse {
    public final long messageId;

    public SendMessageResponse(long messageId) {
        this.messageId = messageId;
    }
}
