package chat.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class LoginRequest {
    @NotNull
    public final String username;

    @JsonCreator
    public LoginRequest(@JsonProperty("username") String username) {
        this.username = username;
    }
}
