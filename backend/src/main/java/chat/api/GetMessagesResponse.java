package chat.api;

import com.google.common.collect.ImmutableList;

import java.util.List;

public class GetMessagesResponse {
    public final List<ChatMessage> messages;

    public GetMessagesResponse(List<ChatMessage> messages) {
        this.messages = ImmutableList.copyOf(messages);
    }
}
