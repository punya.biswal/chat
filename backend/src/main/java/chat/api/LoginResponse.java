package chat.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginResponse {
    public final long userId;

    @JsonCreator
    public LoginResponse(@JsonProperty("userId") long userId) {
        this.userId = userId;
    }
}
