package chat.api;

public class ChatMessage {
    public final long id;
    public final String sender;
    public final String message;

    public ChatMessage(long id, String sender, String message) {
        this.id = id;
        this.sender = sender;
        this.message = message;
    }
}
