package chat.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class SendMessageRequest {
    @NotNull
    public final long userId;

    @NotEmpty
    public final String message;

    public SendMessageRequest(
            @JsonProperty("userId") long userId,
            @JsonProperty("message") String message) {
        this.userId = userId;
        this.message = message;
    }
}
