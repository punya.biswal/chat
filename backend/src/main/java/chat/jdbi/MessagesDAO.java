package chat.jdbi;

import chat.api.ChatMessage;
import org.skife.jdbi.v2.ClasspathStatementLocator;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import org.skife.jdbi.v2.sqlobject.customizers.OverrideStatementLocatorWith;

import java.util.List;

@OverrideStatementLocatorWith(ClasspathStatementLocator.class)
public interface MessagesDAO {
    @SqlUpdate
    void createMessagesTable();

    @SqlUpdate
    @GetGeneratedKeys
    long addMessage(@Bind("userId") long userId, @Bind("message") String message);

    @SqlQuery
    @Mapper(ChatMessageMapper.class)
    List<ChatMessage> getMessagesSince(@Bind("lastSeen") long lastSeen);
}
