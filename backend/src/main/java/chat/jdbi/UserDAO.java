package chat.jdbi;

import org.skife.jdbi.v2.ClasspathStatementLocator;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.OverrideStatementLocatorWith;

@OverrideStatementLocatorWith(ClasspathStatementLocator.class)
public interface UserDAO {
    @SqlUpdate
    @GetGeneratedKeys
    long createUserIfNonexistent(@Bind("username") String username);

    @SqlQuery
    long getUserId(@Bind("username") String username);

    @SqlUpdate
    void createUsersTable();
}
