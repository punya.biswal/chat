package chat.jdbi;

import chat.api.ChatMessage;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ChatMessageMapper implements ResultSetMapper<ChatMessage> {
    @Override
    public ChatMessage map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        return new ChatMessage(r.getLong("id"), r.getString("sender"), r.getString("message"));
    }
}
