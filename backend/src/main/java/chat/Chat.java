package chat;

import chat.jdbi.MessagesDAO;
import chat.jdbi.UserDAO;
import chat.resources.LoginResource;
import chat.resources.MessagesResource;
import io.dropwizard.Application;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Environment;
import org.skife.jdbi.v2.DBI;

public class Chat extends Application<ChatConfiguration> {
    public static void main(String... args) throws Exception {
        new Chat().run(args);
    }

    @Override
    public void run(ChatConfiguration configuration, Environment environment) throws Exception {
        DBIFactory dbiFactory = new DBIFactory();
        DBI dbi = dbiFactory.build(environment, configuration.database, "db");

        UserDAO userDao = dbi.onDemand(UserDAO.class);
        MessagesDAO messagesDao = dbi.onDemand(MessagesDAO.class);

        userDao.createUsersTable();
        messagesDao.createMessagesTable();

        environment.jersey().register(new LoginResource(userDao));
        environment.jersey().register(new MessagesResource(messagesDao));
    }
}
